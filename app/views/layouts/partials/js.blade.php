<script src="assets/js/jquery-1.11.3.min.js"></script>
<!--<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->
<!--Sticky -->
<!--<script type="text/javascript" src="assets/js/jquery.sticky.js"></script>-->
<script src="assets/js/bootstrap.min.js"></script>
<!--Owl Carousel-->
<script type="text/javascript" src="assets/owl-carousel/owl.carousel.min.js"></script>
<!--Parallax-->
<script type="text/javascript" src="assets/js/jquery.stellar.min.js"></script>
<!--IsoTop-->
<script type="text/javascript" src="assets/js/isotope.pkgd.min.js"></script>
<!--Typed js-->
<script type="text/javascript" src="assets/js/typed.js"></script>
<!--Smooth Scroll-->
<script type="text/javascript" src="assets/js/smooth-scroll.js"></script>
<!--Reveal JS-->
<script type="text/javascript" src="assets/js/scrollReveal.min.js"></script>
<!--Main-->
<script type="text/javascript" src="assets/js/main.js"></script>
<!--Whats App-->
<script type="text/javascript" src="assets/js/whatsapp-button.js"></script>
