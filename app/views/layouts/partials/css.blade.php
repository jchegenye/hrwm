  <!--Bootstrap-->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
  <!--FontAwesome-->
  <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">
  <!--Fonts-->
  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
  <!--Owl Carousel-->
  <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css" type="text/css">
  <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css" type="text/css">
  <!--<link rel="stylesheet" href="assets/owl-carousel/owl.transitions.css" type="text/css">-->
  <!--Main Style-->
  <link rel="stylesheet" href="style.css" type="text/css">

  <!--Responsive Style-->
  <link rel="stylesheet" href="assets/css/responsive.css" type="text/css">
  <style>
    [data-sr] {
      visibility: hidden;
    }
  </style>