<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf_token" content="{{ csrf_token() }}">

<title>Hope And Restoration Worship Ministries | (HRWM)</title>

<meta name="description" content="A christian church located in Juja Town.">
<meta name="keywords" content="kenya, hope, restoration, worship, ministries">
<meta name="revisit-after" content="7 days">
<meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
<meta name="author" content="Chegenye Asumu Jackson">
<meta name="reply-to" content="chegenyejackson@gmail.com">
<meta name="web_author" content="https://j-tech.tech">
<meta name="abstract" content="Hope And Restoration Worship Ministries | (HRWM)">
<meta name="distribution" content="global">
<meta name="robots" content="none, noindex,nofollow">
<meta name="googlebot" content="all, index, follow">
<meta name="language" content="english, kiswahili">
<meta name="rating" content="general, safe for kids">
<meta name="identifier-URL"CONTENT="https://hopeandrestorationworshipministries.org/">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<!-- Favicon -->
<link rel="icon" href="{{ asset('http://hopeandrestorationworshipministries.org/favicon.ico') }}">
