<!DOCTYPE html>
<html lang="en">
<head>
    <title>Kenya Professional Society Of Criminology | Support</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style type="text/css">
        @import url("http://fonts.googleapis.com/css?family=Raleway:200,300,600,700&subset=latin,latin-ext");
        body, table{
            font-family:cursive;
        }
        .appleFooter a {
            color:rgb(0, 81, 119); 
            font-size:12px;
            text-decoration: none;
        }
    </style>
</head>
<body style="margin: 0; padding: 0; background:rgb(245, 245, 245);">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <div
                    style="display: none; font-size: 1px; color:rgb(0, 81, 119); line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

            </div>
              <table border="0" cellpadding="0" cellspacing="0" width="620" class="wrapper">
                  <tr>
                      <td style="padding: 10px 0px 10px 0px;" class="logo">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                  <td width="100%" align="left">
                                      <a href="#" target="_blank">
                                          <a style="
                                          display: block;
                                          color:rgb(0, 81, 119);
                                          font-size: 16px;
                                          cursor: pointer;"
                                             border="0">

                                          </a>
                                      </a>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
              </table>
        </td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td style="
      opacity: 0.4;
      padding: 20px 20px;
      background-color:rgb(98, 154, 173)" >
          <a href="http://www.hopeandrestorationworshipministries.org/" style="text-decoration: none;
  color: #FFFFFF;
  font-weight: 700;
  letter-spacing: 1.2em;font-size: 25px;">HRWM </a>
      </td>
    </tr>
    <tr>
        <td bgcolor="color:rgb(0, 81, 119)" align="center" style="padding:20px 20px; background-color: #fff;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left"
                                                style="color: #675C5C; padding-bottom: 1.2em;"
                                                class="padding-copy">Dear h r w m,
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: #675C5C;">
                                                My email is {{$email}}<b></b> and phone number {{$phone_number}}.
                                                <br>
                                                Subject: {{$subject}}<b></b><br>
                                                Message: {{$message}}<b></b>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left"
                                                style="color: #675C5C; padding-top: 1.2em;"
                                                class="padding-copy">Regards,<br>{{$full_names}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color: #675C5C; padding-top: 1.1em; ">
                                                -------------------------------------<br>
                                                Support token: {{$_token}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center" style="background:rgb(249, 249, 249);">
    <tr>
        <td align="center" style="padding:0px 20px;">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td align="left"
                                    style="padding: 20px 0 0 0; font-size: 12px; color: #666666;"
                                    class="padding-copy">
                                    <span class="appleFooter" style="color:#666666;">
                                    <a href="j-tech.tech">j-tech.tech</a> {{date('Y')}}</span><br><span
                                            class="original-only"
                                            style=" color: #444444;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>