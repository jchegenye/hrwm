<!--/HEADER SECTION -->
<header class="header">
  <div class="container">
    <div class="navbar navbar-default" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand " href="{{'/'}}">
            <span class="logo-text">HRWM</span>
            <!-- <img src="logo.png" alt="LOGO" style="height: inherit;"> -->
          </a>
        </div><!-- end navbar-header -->
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a class="active" href="{{'/'}}" >Home</a></li>
            <li><a data-scroll href="{{'/about-us'}}" >About</a></li>
            <li><a data-scroll href="{{'/gallery'}}" >Gallery</a></li>
            <li><a data-scroll href="{{'/events'}}" >Events</a></li>
            <li><a data-scroll href="{{'/testimonial'}}" >Testimonial</a></li>
            <li><a data-scroll href="{{'/contact'}}" >Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div><!--/.container-fluid -->
    </div>
  </div><!-- end container -->
</header><!-- end header -->