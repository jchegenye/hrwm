<!DOCTYPE html>
<html lang="en">
	<head>
	  @include('layouts.partials.meta-tags')
	  @include('layouts.partials.css')
	</head>

	@include('pages.header')

<body>
<section id="latest-works">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h1 class="section-title">Latest Works</h1>
			</div>
		</div>
		<div class="row text-center">
			<div class="works-category"  data-sr='enter top, wait 0.2s'>
				<ul class=" list-inline">
					<li><a href="#" data-filter="*" class="current">All</a></li>
					<li><a href="#" data-filter=".branding">revival meetings</a></li>
					<li><a href="#" data-filter=".design">open air crusade</a></li>
					<li><a href="#" data-filter=".development">out reach</a></li>
					<li><a href="#" data-filter=".strategy">sunday service</a></li>
				</ul>
			</div>
			<div class="works-area" >
				<div class="col-md-4 col-sm-6 col-xs-12  branding"  >
					<div class="works">
						<img src="assets/images/sample.jpg" alt="">

						<div class="work-overlay text-center">
							<div class="overlay-caption">
								<h4>Title</h4>
								<p>Description</p>
								<a class="btn-view" href="#">VIEW <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 design" >
					<div class="works">
						<img src="assets/images/sample.jpg" alt="">

						<div class="work-overlay text-center">
							<div class="overlay-caption">
								<h4>Title</h4>
								<p>Description</p>
								<a class="btn-view" href="#">VIEW <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 development" >
					<div class="works">
						<img src="assets/images/sample.jpg" alt="">

						<div class="work-overlay text-center">
							<div class="overlay-caption">
								<h4>Title</h4>
								<p>Description</p>
								<a class="btn-view" href="#">VIEW <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 design" >
					<div class="works">
						<img src="assets/images/sample.jpg" alt="">

						<div class="work-overlay text-center">
							<div class="overlay-caption">
								<h4>Title</h4>
								<p>Description</p>
								<a class="btn-view" href="#">VIEW <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 strategy" >
					<div class="works">
						<img src="assets/images/sample.jpg" alt="">

						<div class="work-overlay text-center">
							<div class="overlay-caption">
								<h4>Title</h4>
								<p>Description</p>
								<a class="btn-view" href="#">VIEW <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 branding design" >
					<div class="works">
						<img src="assets/images/sample.jpg" alt="">

						<div class="work-overlay text-center">
							<div class="overlay-caption">
								<h4>Title</h4>
								<p>Description</p>
								<a class="btn-view" href="#">VIEW <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 strategy" >
					<div class="works">
						<img src="assets/images/sample.jpg" alt="">

						<div class="work-overlay text-center">
							<div class="overlay-caption">
								<h4>Title</h4>
								<p>Description</p>
								<a class="btn-view" href="#">VIEW <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 development" >
					<div class="works">
						<img src="assets/images/sample.jpg" alt="">

						<div class="work-overlay text-center">
							<div class="overlay-caption">
								<h4>Title</h4>
								<p>Description</p>
								<a class="btn-view" href="#">VIEW <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 branding" >
					<div class="works">
						<img src="assets/images/sample.jpg" alt="">

						<div class="work-overlay text-center">
							<div class="overlay-caption">
								<h4>Title</h4>
								<p>Description</p>
								<a class="btn-view" href="#">VIEW <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
	</div>
</section>

@include('pages.footer')
@include('layouts.partials.js')