<!DOCTYPE html>
<html lang="en">
	<head>
	  @include('layouts.partials.meta-tags')
	  @include('layouts.partials.css')
	</head>

	@include('pages.header')

<body>
<section id="recent-posts">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h1 class="section-title">Recent / Upcoming events</h1>
				<p class="sec-subtitle">
					Up coming church events for the new year 2016. Would you like to receive this events on you email? <a href="{{'/footer'}}">Click here.</a>
				</p>
			</div>
		</div>
		<div class="posts-area"  data-sr="enter left, hustle 60px">
			<div class="row">
				<div class="col-sm-5 post-thumb">
					<img src="assets/images/events/1.jpg" alt="" class="img-responsive">
				</div>
				<div class="col-sm-7 post-content">
					<div class="post-header clearfix">
						<div class="post-date pull-left text-center">
							<h1>{{date('d')}}</h1>
							<p>{{date('M')}}</p>
							<p>
								{{date('Y')}}
							</p>
						</div>
						<div class="post-title pull-left">
							<h4><a href="#">revival meetings</a></h4>
							<p>By Pastor. <a href="{{'profile'}}">John Isaji</a></p>
							<p>
								<a href="#"><i class="fa fa-facebook"></i></a> &nbsp &nbsp
								<a href="#"><i class="fa fa-twitter"></i></a> &nbsp &nbsp
								<a href="#"><i class="fa fa-google-plus"></i></a> &nbsp &nbsp
								<a href="#"><i class="fa fa-whatsapp"></i></a>
							</p>
						</div>
					</div>
					<div class="post-body clearfix">
						<p>
							A revival meeting usually consists of several consecutive nights of services conducted at the same time and location, most often the building belonging to the sponsoring congregation but sometimes a rented assembly hall, for more adequate space, to provide a setting that is more comfortable for non-Christians, or to reach a community where there are no churches. Tents were very frequently employed in this effort in the recent past, and occasionally still are, but less so due to the difficulties in heating and cooling them and otherwise making them comfortable, an increasing consideration with modern audiences.
						</p>
						<p><a href="#">Read More</a></p>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

@include('pages.footer')
@include('layouts.partials.js')