<!DOCTYPE html>
<html lang="en">
	<head>
	  @include('layouts.partials.meta-tags')
	  @include('layouts.partials.css')
	</head>

	@include('pages.header')

<body>

<section id="about-us">
	<div class="container">
		<div class="row ">
			<div class="col-sm-12 text-center">
				<h2 class="section-title">About us</h2>

				<p class="sec-subtitle">Lorem ipsum dolor sit amet, consetetur sadipscing elitr amet</p>
			</div>
			<div class="col-sm-6">
			<h5>Who we are?</h5>
				<p>
					We are a christian church located in Juja Town, Nairobi Kenya.
				</p>
			</div>
			<div class="col-sm-6">
			<h5>Our Mission</h5>
				<p>
					Hope and restoration Worship ministries works to contribute to the spiritual growth and providing Biblical Enrichment, Spiritual Guidance, Motivational Resource  and economic empowerment.
				</p>
				<p>
					Pastor John Isaji believes that his ministry will enable Christians to overcome personal obstacles and limitations and adapt a zeal for advancement.
				</p>
			</div>

			<div class="col-sm-12" ><h5>Our Beliefs</h5>
				<div id="owl-demo" style="    text-align: justify;">
					<div class="item" data-sr="enter left, hustle 60px">
					
						<div class="owl-caption">
							<h5>The Bible</h5>
							<p>
								The scriptures, both Old and New Testaments, are the inspired word of God with writings, and complete revelation of His will for the salvation of men and the divine and final authority for all Christian faith and life.
							</p>
						</div>

					</div>
					<div class="item " data-sr="enter left, hustle 60px">

						<div class="owl-caption">
							<h5>God</h5>
							<p>
								There is one God, Creator of all things, infinitely perfect and eternally existing in Father, Son and Holy spirit.
							</p>
						</div>

					</div>
					<div class="item " data-sr="enter right, hustle 60px">

						<div class="owl-caption">
							<h5>Jesus Christ</h5>
							<p>
								Jesus Christ is the true man, having been conceived of the Holy spirit and born of virgin Mary. He died on  the cross, the complete and final sacrifice for our sins according to the scriptures.
							</p>
						</div>

					</div>
					<div class="item " data-sr="enter right, hustle 60px">

						<div class="owl-caption">
							<h5>The holy spirit</h5>
							<p>
								The Ministry of the Holy Spirit is to Glorify the Lord Jesus Christ and during this age to convict men of sin regenerate the believing sinner, indwell, guide, instruct and empower the  believer  for Godly living and service.
							</p>
						</div>

					</div>
					<div class="item ">

						<div class="owl-caption">
							<h5>Salvation</h5>
							<p>
								The shed blood of Jesus Christ and his resurrection provide the only ground for justification and salvation for all who believe, and only such as receive Jesus Christ by faith are born of the Holy spirit and they become children of God.
							</p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>

</body>
</html>

@include('pages.footer')
@include('layouts.partials.js')
