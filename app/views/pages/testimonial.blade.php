<!DOCTYPE html>
<html lang="en">
	<head>
	  @include('layouts.partials.meta-tags')
	  @include('layouts.partials.css')
	</head>

	@include('pages.header')

<body>

<section id="testimonial">
	<div class="container text-center">
		<div class="row ">
			<div class="col-sm-12">
				<ul class="list-inline clients">
					<span class="section-title" data-sr='enter top, wait 0.2s'>
						Testimonial
					</span>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1"  data-sr='enter bottom, wait 0.2s'>
				<div id="owl-testimonial" class="owl-carousel">
					<div class="item">
						<div class="testimonials">
						<span data-sr='enter top, wait 0.2s'>
							<img src="assets/images/mmedia.png" alt="">
						</span>
							<p class="testimonial-text">"Description ”</p>
							<p class="testimonial-author">name, company, organisation</p>
						</div>
					</div>
					<div class="item">
						<div class="testimonials">
							<p class="testimonial-text">"Description ”</p>
							<p class="testimonial-author">name, company, organisation</p>
						</div>
					</div>
					<div class="item">
						<div class="testimonials">
							<p class="testimonial-text">"Description ”</p>
							<p class="testimonial-author">name, company, organisation</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- / Container-->
</section>
<!-- / Testimonial-->

@include('pages.footer')
@include('layouts.partials.js')