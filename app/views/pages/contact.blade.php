<!DOCTYPE html>
<html lang="en">
	<head>
	  @include('layouts.partials.meta-tags')
	  @include('layouts.partials.css')
	</head>

	@include('pages.header')

<body>

<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h1 class="section-title">Contact</h1>

				<p class="sec-subtitle">
					Got a question? Don’t keep it to yourself. Tell us what it is and we’ll show you how to get the answer as quickly as possible.
				</p>
			</div>
			<div class="col-sm-4" data-sr="enter left, hustle 80px">
				<div class="address">
					<p class="address-title title"><i class="fa fa-map-marker"></i> Address</p>

					<p class="address-content"> P.o Box 54655-00200<br>
						 (city Sq) Nairobi </p>
				</div>
				<div class="address">
					<p class="address-title title"><i class="fa fa-mobile-phone">
					</i> Hotline (24/7Hrs)</p>

					<p class="address-content"> +254 722 103 623 or +732 228 583</p>
				</div>
				<div class="address">
					<p class="address-title title"><i class="fa fa-envelope-o"></i> E-mail</p>
					<p class="address-content">isaji@hrwm.org<br>isaji@gmail.com</p>
				</div>
			</div>
			<div class="col-sm-8" data-sr="enter right, hustle 80px">
				@if(Session::has('successfull_enquiry'))
					<div class="alert alert-success">
						<p>{{ Session::get('successfull_enquiry') }}</p>
					</div>
				@endif
				<form class="st-form" method="POST" action="/contact-us">
					<input type="hidden" value="{{ csrf_token() }}" name="_token">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group ">
								<input type="text" name="full_names" class="form-control" placeholder="YOUR NAME">
								@if ($errors->has('full_names'))
									<p class="text text-danger">
										{{ $errors->first('full_names') }}
									</p>
								@endif
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group ">
								<input type="text" name="phone_number" class="form-control" placeholder="YOUR PHONE NUMBER">
								@if ($errors->has('phone_number'))
									<p class="text text-danger">{{ $errors->first('phone_number') }}</p>
								@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" name="email" class="form-control" placeholder="YOUR E-MAIL">
								@if ($errors->has('email'))
									<p class="text text-danger">{{ $errors->first('email') }}</p>
								@endif
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group ">
								<input type="text" name="subject" class="form-control" placeholder="YOUR SUBJECT">
								@if ($errors->has('subject'))
									<p class="text text-danger">{{ $errors->first('subject') }}</p>
								@endif
							</div>
						</div>
					</div>
					<div class="form-group ">
						<textarea class="form-control" name="message" rows="4" placeholder="WRITE YOUR MESSAGE"></textarea>
						@if ($errors->has('message'))
							<p class="text text-danger">{{ $errors->first('message') }}</p>
						@endif
					</div>
					<button type="submit" class="btn btn-send">Sign in</button>
				</form>
			</div>
		</div>
	</div>
</section>

@include('pages.footer')
@include('layouts.partials.js')