<section id="get-in-touch" data-stellar-background-ratio="0.7">
	<div class="action-overlay"></div>
	<div class="container">
		<div class="row ">
			<div class="col-md-6"   data-sr="enter top, hustle 60px">
			@if(Session::has('successfulSubscription'))
		      <div class="alert alert-success">
		        {{ Session::get('successfulSubscription') }}
		      </div>
		    @endif

		    @if(Session::has('blankSubscription'))
		      <div class="alert alert-error">
		        {{ Session::get('blankSubscription') }}
		      </div>
		    @endif
				<h4 class="action-title">Subscribe</h4>
      			<p class="sub-head">Would you like to receive our newsletter?</p>
					<form action="{{'/subscribe'}}" class="input-group input-group-lg" id="subscribeForm" method="POST">
		              	<span class="input-group-addon get-in-touch-letter-icon">
		              		<i class="fa fa-envelope"></i>
		              	</span>
		               	<input type="hidden" value="{{ csrf_token() }}" name="_token">

		            	<input type="email" class="form-control" data-validation="email" id="sEmail" name="sEmail" placeholder="Enter your email address">

		              	<span class="input-group-btn">
		                	<button class="btn btn-info" type="submit">Go</button>
		              	</span>                   
	            	</form>
			</div>
			<div class=" has-warning @if ($errors->has('sEmail')) has-error @endif">    
                @if ($errors->has('sEmail')) 
                  <p class="help-block">{{ $errors->first('sEmail') }}</p> 
                @endif           
            </div>
		</div>
	</div>
</section>

<!--Footer-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center" data-sr='enter bottom, wait 0.32s'>
				<div class="social">
					<ul class="list-inline">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>

						<li></li>
					</ul>
				</div>

				<p>&copy; {{date('Y')}} Hope And Restoration Worship Ministries | Designed by <a href="https://j-tech.tech">J-tech Co.</a></p>
			</div>
		</div>
	</div>
</footer>
<!-- / Footer-->