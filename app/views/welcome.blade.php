<!DOCTYPE html>
<html lang="en">
<head>
  @include('layouts.partials.meta-tags')
  @include('layouts.partials.css')
</head>
<body>

  @include('layouts.modals.video-profile')

  @include('pages.header')

<!--/SLIDER SECTION -->
<section id="home" class="sliderwrapper clearfix">

  <div class="home-overlay"></div>

  <div class="container">
    <div class="row text-center">
      <div class="col-md-8 col-md-offset-2">
        <div class="block" >
          <h1 class="home-title wow fadeInDown"> <span class=element></span></h1>

          <p class="title-small wow fadeInDown" data-wow-delay="0.3s">A is a christian church located in juja town along thika road</p>

          <div class="wow fadeInDown" data-wow-delay="0.3s">
            <a class="btn btn-action" href="#" data-toggle="modal" data-target="#myModal"
               role="button">Watch Video
            </a>
          </div>

        </div>
      </div>
      <!-- .row close -->
    </div>
  </div>

</section><!-- end Home -->

@include('layouts.partials.js')

</body>
</html>