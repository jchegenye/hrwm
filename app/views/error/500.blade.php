
<title>H&RWM - Error500 | {{$segment = Request::segment(1);}}</title>

<link rel="icon" href="{{ asset('/favicons/favicon.ico') }}">

@include('layouts.landing-page.partials.css-js-links')

  <div class="container">   
    <div class="col-md-12">

      <div style="padding-top: 150px; margin: 0 5%;">

        <div style="width: 46%; float:left; font-size: 132px; color: #099; line-height: .8em;">500</div> 
        
          <div style="width:54%; float:left;">
            <h2>Oops! Don't panic!</h2>
            <p style="font-size:23px;">Something went wrong.</p>
          </div>

          <div style="border-bottom:1px solid teal; padding:10px;">
            <h6>
              Dont worry
            </h6>
          </div>

          <div style="border-bottom:1px solid teal; padding:10px;">
            <ul>
              <li>We will work on fixing that right away. Meanwhile, 
              you may try using the search form below.</li>
            </ul>

            <div class="">Try here 
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for..." value="{{$segment = Request::segment(1);}}">
                <span class="input-group-btn">
                  <a href="{{'#'}}" class="btn btn-default" type="button">Search!</a>
                </span>
              </div>
            </div>
          </div>

          <div style="padding:10px;">
            We can help you find what your looking for, try the following.
          </div>

          <div>
            <a href="{{'/'}}" style="padding-right:10px;">Go Back Home to Page</a> | 
            <a href="{{'/#contact'}}" style="padding-right:10px; padding-left:10px;">Contact Us</a> | 
            <a href="{{'/#faq'}}" style="padding-right:10px; padding-left:10px;">FAQ</a> | 
            <a href="{{'/#help'}}" style="padding-right:10px; padding-left:10px;">HELP</a>
          </div>

        </div>

      </div>
  </div>