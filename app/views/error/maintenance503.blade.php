<style>
  body {
      background: #f8f8f8 url(../assets/maintenancePic/pattern.jpg);
      border-top: 2px solid #e75967;
      font-family: 'Lato', sans-serif;
      color: #555;
      font-weight: 400;
      text-align: left;
  }

  strong { font-weight: 700; }

  a, a:hover, a:focus {
    color: #e75967;
    text-decoration: none;
      -o-transition: all .3s; -moz-transition: all .3s; -webkit-transition: all .3s; -ms-transition: all .3s; transition: all .3s;
  }

  img { max-width: 100%; }

  ::-moz-selection { background: #e75967; color: #fff; text-shadow: none; }
  ::selection { background: #e75967; color: #fff; text-shadow: none; }


  /***** Header *****/

  .logo h1 {
/*      margin-top: 7px;
      font-family: 'Lobster', cursive;
      font-size: 38px;
      line-height: 42px;
      font-weight: 400;
      color: #555;*/

      font-size: 18px;
      line-height: 22px;
      text-align: right;
  }

  .logo h1 span { color: #e75967; }

  .logo a { color: #555; }
  .logo a:hover { color: #e75967; }

  .call-us {
      font-size: 18px;
      line-height: 22px;
      text-align: right;
  }

  .call-us p {
      margin-top: 18px;
  }

  .call-us p span { color: #888; }


  /***** Coming Soon *****/

  .coming-soon {
      margin: 0 auto;
      text-align: center;
      color: #fff;
  }

  .inner-bg {
      padding: 55px 0 60px 0;
      background: url(../assets/maintenancePic/pattern-3.png);
      -moz-box-shadow: 0 1px 5px 0 rgba(0,0,0,.3) inset;
      -webkit-box-shadow: 0 1px 5px 0 rgba(0,0,0,.3) inset;
      box-shadow: 0 1px 5px 0 rgba(0,0,0,.3) inset;
  }

  .coming-soon h2 {
      font-size: 40px;
      line-height: 44px;
      font-weight: 700;
      text-transform: uppercase;
      text-shadow: 0 1px 7px rgba(0,0,0,.2);
  }

  .coming-soon p {
    margin-top: 10px;
    font-size: 26px;
    /* line-height: 36px; */
    text-shadow: 0 1px 7px rgba(0,0,0,.2);
    color: black;
  }

  .timer {
      margin-top: 40px;
      text-shadow: 0 1px 5px rgba(0,0,0,.1);
  }

  .timer .days-wrapper,
  .timer .hours-wrapper,
  .timer .minutes-wrapper,
  .timer .seconds-wrapper {
      display: inline-block;
      width: 160px;
      height: 160px;
      margin: 0 10px;
      padding-top: 20px;
      background: #2d2d2d; /* browsers that don't support rgba */
      background: rgba(45, 45, 45, 0.7);
      font-size: 18px;
      line-height: 22px;
      -moz-border-radius: 50%; -webkit-border-radius: 50%; border-radius: 50%;
  }

  .timer .days-wrapper:hover,
  .timer .hours-wrapper:hover,
  .timer .minutes-wrapper:hover,
  .timer .seconds-wrapper:hover {
      background: #e75967 url(../assets/maintenancePic/pattern-2.png);
      text-shadow: none;
  }

  .timer .days,
  .timer .hours,
  .timer .minutes,
  .timer .seconds {
      font-size: 80px;
      line-height: 90px;
  }


  /***** Content *****/

  .subscribe {
      margin-top: 30px;
      text-align: center;
  }

  .subscribe h3 {
      font-size: 32px;
      font-weight: 400;
      color: #4d4d4d;
      line-height: 40px;
      text-transform: uppercase;
      text-shadow: 1px 2px 1px #fff;
  }

  .subscribe p {
      font-size: 18px;
      font-weight: 400;
      line-height: 36px;
  }

  .subscribe form {
      margin-top: 24px;
  }

  .subscribe form input.subscribe-email {
      width: 310px;
      height: 46px;
      margin: 0;
      padding: 0 10px;
      background: #fff;
      font-family: 'Lato', sans-serif;
      font-size: 18px;
      line-height: 46px;
      color: #888;
      border: 0;
      -moz-border-radius: 0; -webkit-border-radius: 0; border-radius: 0;
      -moz-box-shadow: 0 1px 3px 0 rgba(0,0,0,.15);
      -webkit-box-shadow: 0 1px 3px 0 rgba(0,0,0,.15);
      box-shadow: 0 1px 3px 0 rgba(0,0,0,.15);
  }

  .subscribe form input:focus {
      -moz-box-shadow: 0 1px 3px 0 rgba(0,0,0,.15);
      -webkit-box-shadow: 0 1px 3px 0 rgba(0,0,0,.15);
      box-shadow: 0 1px 3px 0 rgba(0,0,0,.15);
  }

  .subscribe form input:-moz-placeholder { color: #888; }
  .subscribe form input:-ms-input-placeholder { color: #888; }
  .subscribe form input::-webkit-input-placeholder { color: #888; }

  .subscribe form button {
      width: 130px;
      height: 46px;
      margin: 0;
      padding: 0;
      background: #e75967;
      border: 0;
      font-family: 'Lato', sans-serif;
      font-size: 18px;
      line-height: 46px;
      color: #fff;
      text-transform: uppercase;
      text-shadow: 1px 1px 1px rgba(0,0,0,.3);
      -moz-border-radius: 0; -webkit-border-radius: 0; border-radius: 0;
      -moz-box-shadow: 0 1px 3px 0 rgba(0,0,0,.25);
      -webkit-box-shadow: 0 1px 3px 0 rgba(0,0,0,.25);
      box-shadow: 0 1px 3px 0 rgba(0,0,0,.25);
      -o-transition: all .3s; -moz-transition: all .3s; -webkit-transition: all .3s; -ms-transition: all .3s; transition: all .3s;
  }

  .subscribe form button.btn:hover {
      background: #555;
      color: #fff;
  }

  .subscribe form button.btn:focus, .subscribe form button.btn:active {
    outline: 0;
      background: #555;
      color: #fff;
  }

  .success-message, .error-message {
    display: none;
    margin-top: 20px;
      font-size: 18px;
      line-height: 22px;
  }

  .error-message {
      color: #e75967;
  }

  .social {
      margin-top: 50px;
      padding-bottom: 50px;
      text-align: center;
  }

  .social a {
      display: inline-block;
      width: 48px;
      height: 48px;
      margin: 3px;
      padding-top: 9px;
      background: #555;
      -moz-border-radius: 50%; -webkit-border-radius: 50%; border-radius: 50%;
      font-size: 28px;
      color: #fff;
      line-height: 28px;
  }

  .social a:hover, .social a:focus { outline: 0; background: #e75967; }


  /***** Media Queries *****/

  @media (min-width: 768px) and (max-width: 991px) {
    
    .timer .days-wrapper,
      .timer .hours-wrapper,
      .timer .minutes-wrapper,
      .timer .seconds-wrapper { width: 140px; height: 140px; margin: 0 7px; }
      
      .timer .days,
      .timer .hours,
      .timer .minutes,
      .timer .seconds { font-size: 60px; line-height: 70px; }
    
  }

  @media (max-width: 767px) {

      .logo h1 { text-align: center; }
      .call-us { padding-bottom: 7px; text-align: center; }

      .inner-bg { padding: 45px 0 50px 0; }

      .coming-soon h2 { font-size: 36px; }

      .timer .days-wrapper,
      .timer .hours-wrapper,
      .timer .minutes-wrapper,
      .timer .seconds-wrapper { width: 140px; height: 140px; margin: 7px; }

      .timer .days,
      .timer .hours,
      .timer .minutes,
      .timer .seconds { font-size: 60px; line-height: 70px; }

      .subscribe h3 { font-size: 28px; }
      .subscribe form input.subscribe-email, .subscribe form button { width: 100%; }

  }
</style>

<head>
  <title>Maintenance | Hope And Restoration Worship Ministries - Juja Town</title>

  @include('layouts.landing-page.partials.css-js-links')
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Lato:400,700'>
</head>

<body>
<!-- Header -->
<div class="container">
    <div class="row header">
        <div class="col-sm-6 logo">
          <p>Hope & Restoration Worship Ministry <span>.</span></p>
            <!-- <h4><a href="index.html">Hope & Restoration Worship Ministry</a> <span>.</span></h4> -->
        </div>
        <div class="col-sm-6 call-us">
            <p>Tel: <span>+254 711 494 289</span> | Facebook: <span>info@hrwm.com</span></p>
        </div>
    </div>
</div>

<!-- Coming Soon -->
<div class="coming-soon">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>We're Coming Soon</h2>
                    <p>We are working very hard on the new version of our site. It will bring a lot of new features. Stay tuned!</p>
                    <div class="timer">
                        <div class="days-wrapper">
                            <span class="days"></span> <br>days
                        </div>
                        <div class="hours-wrapper">
                            <span class="hours"></span> <br>hours
                        </div>
                        <div class="minutes-wrapper">
                            <span class="minutes"></span> <br>minutes
                        </div>
                        <div class="seconds-wrapper">
                            <span class="seconds"></span> <br>seconds
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Content -->
<div class="container">
    <div class="row">
    @if(Session::has('successfulSubscription'))
      <div class="alert alert-success">
        {{ Session::get('successfulSubscription') }}
      </div>
    @endif

    @if(Session::has('blankSubscription'))
      <div class="alert alert-error">
        {{ Session::get('blankSubscription') }}
      </div>
    @endif
      <div class="col-sm-12 subscribe">
          <h3>Subscribe to our newsletter</h3>
          <p>Sign up now to our newsletter and you'll be one of the first to know when the site is ready:</p>                    
          <form class="form-inline" role="form" action="{{'/subscribe2'}}" method="post">
            <div class="form-group">
              <label class="sr-only" for="subscribe-email">Email address</label>
                <input type="text" name="email" placeholder="Enter your email..." class="subscribe-email form-control" id="subscribe-email">
              </div>
              <button type="submit" class="btn">Subscribe</button>
          </form>
      </div>
      <div class=" has-warning @if ($errors->has('sEmail')) has-error @endif">    
        @if ($errors->has('sEmail')) 
          <p class="help-block">{{ $errors->first('sEmail') }}</p> 
        @endif           
      </div>
    </div>

    <div class="row">
        <div class="col-sm-12 social">
            <a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
            <a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
            <a href="#" data-toggle="tooltip" data-placement="top" title="Dribbble"><i class="fa fa-dribbble"></i></a>
            <a href="#" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
            <a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a>
            <a href="#" data-toggle="tooltip" data-placement="top" title="Flickr"><i class="fa fa-flickr"></i></a>
        </div>
    </div>
</div>


<!--[if lt IE 10]>
    <script src="assets/js/placeholder.js"></script>
<![endif]-->

    </body>