<?php 
/**
 * Handles the user login.
 *
 */
use Hivisasa\User\Logout;
//use Session;
use Response;
use Redirect;

class LoginController extends BaseController {
  
  //Login Page
  public function showLogin(){
    //Check if the redirect parameter is set explicitly
    if (Agent::isMobile()) {
      if (Agent::version('Opera Mini') || Agent::mobileGrade() == 'C' || Agent::mobileGrade() == 'B') {
        return View::make('users.opera_mini.login');
      } 
    }
    return View::make('users.login');
  }

  public function doLogin() {    
    $rules = array(
      'email' => 'required|email|max:255',
      'password' => 'required|max:255',
    );

    $validator = Validator::make(Input::all(), $rules);

    if ($validator->fails()) {
      return Redirect::back()
      ->withErrors($validator)
      ->withInput(Input::except('password'));
    }
    else{
      // create our user data for the authentication
      $userdata = array(
        'email'   => Input::get('email'),
        'password'  => Input::get('password')
      );

      // attempt to do the login
      if (Auth::attempt($userdata,true)) {

        $user['email'] = Auth::user()->email;
        $user['username'] = Auth::user()->username;
        $user['uid'] = Auth::user()->uid;
        $user['id'] = Auth::user()->id;
        
        $user_data = json_encode($user);
        
        return Redirect::intended('/workbench/' . Auth::user()->uid)
          ->withCookie(Cookie::make('user_cookie', $user_data,600000));
      }
      else {
        // validation not successful, send back to form
        return Redirect::to('login')
          ->withErrors(['unsuccessful_login' => 'Login not successful'])
          ->withInput(Input::except('password'));

      }
    }
  }

  public function doLogout()
  {
    Auth::logout();
    $cookie = Cookie::forget('user_cookie');
    return Redirect::to('/')->withCookie($cookie);
  }

 
}
