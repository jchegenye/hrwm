<?php
class PasswordController extends BaseController {
 
 	/*
 	 Display a form to request for a password reminder.
 		*/
  public function remind()
  {
    return View::make('users.password.remind');
  }

}