<?php
/**
 * This controller is responsible for managing user data and the presentation.
 *
 * Some of its main functionality has been stripped down to smaller traits.
 *
 * Additional support files need to go under Jackchegenye\User\Support\Controller namespace
 *
 * @todo This file needs to be split into smaller components for manageability.
 *
 * @todo We need to find a way of easily versioning controllers.
 *
 * @todo Move all the traits under the Jackchegenye\User\Support\Controller namespace
 *
 */

use Jackchegenye\User\Repository\UserRepositoryEloquent as Repo;

class UsersController extends BaseController
{


  function __construct(Repo $repo){

    $this->repo = $repo;

  }

  /**
   * @param $id Integer Unique user id.
   *
   * @return Illuminate\Support\Facades\View
   */
  public function editRoles($id)
  {
    $user = User::where('uid', '=', intval($id))->first();

    $roles = ['' => 'Choose Roles'] + Role::lists('name', '_id');

    $data = array(
      'user' => $user,
      'roles' => $roles,
    );

    return View::make('admin/workbench')->with($data);
  }

  /**
   * @param $id
   *
   * @return \Illuminate\Http\RedirectResponse
   */
  public function storeUserRole($id)
  {
    $user = User::where('uid', '=', intval($id))->first();
    $user->role_ids = Input::get('role_ids');
    $user->save();

    return Redirect::to('/workbench/' . $id . '/users');
  }
}