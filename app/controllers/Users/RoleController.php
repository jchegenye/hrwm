<?php

class RoleController extends BaseController{

  public function showRole($id){
    $user = Auth::user();
    $role = Role::find($id);

    $assigned_permissions = $role->permission_id;

    $permissions_array = array();
    
    for ($i=0; $i < count($assigned_permissions); $i++) { 
      //Load a permission object
      $permission = UserPermission::find($assigned_permissions[$i]);
      $permission_data['name'] = $permission->name;
      $permission_data['description'] = $permission->description;
      array_push($permissions_array, $permission_data);
    }

    $data = array(
      'user'=>$user,
      'role'=>$role,
      'assigned_permissions' => $permissions_array,
    );
    return View::make('admin/workbench')->with($data);
  }

  public function editPermission($id){
    $user = Auth::user();
    $role = Role::find($id);
    $permissions = ['' => 'Choose Permissions'] + UserPermission::lists('name','_id');
    
    $data = array(
      'user'=>$user,
      'role'=>$role,
      'permissions' => $permissions,
    );
    return View::make('admin/workbench')->with($data);
  }

  /**
   * Update the permissions of the role.
   */
  public function updatePermission($id){
    if (Cache::has('role_' . $id))
    {
      $role = Cache::get('role_' . $id);
    }
    else{
      $role = Role::find($id);

      Cache::put('role_' . $id, $role, 360);
    }

    $role->permission_id = Input::get('permission_id');
    $role->save();

    Cache::forget('role_' . $id);

    return Redirect::to('workbench/role/' . $role->id);
  }

  /**
   * Create a role.
   */
  public function create_role(){
    
    $user = Auth::user();
    $roles = Role::all();

    $data = array(
      'roles'=>$roles,
      'user' => $user
    );

    return View::make('admin/workbench')
    	->with($data);
  }

  /**
   * Store a new role.
   */
  public function store_role(){

  	$rules = ['name' => 'required|unique:roles'];
    
    $validator = Validator::make(Input::all(),$rules);

    if ($validator->fails()) {
      $error = [
        'message' => 'error',
        'message_description' => 'There were errors in your form.',
        'errors' => $validator->messages()
      ];
     
      return Redirect::to('workbench/role/create')
        ->withErrors($validator)
        ->withInput(Input::all());
    } 
    else {

    	$role = new Role;
	    $role->name = Input::get('name');
	    $role->save();

    Session::flash('success_role', 'Role has been added');
        return Redirect::to('workbench/role/create');

    } 
  }
}