<?php

/**
 * Handles the user signup.
 *
 */

use Jackchegenye\User\Signup;
use Jackchegenye\User\Signup\ProcessSignup;
use Jackchegenye\User\Signup\ProcessUserDetails;

class SignupController extends BaseController{
  
  use ProcessSignup,ProcessUserDetails;

  /**Render the signup page.**/
  public function signup(){
    $signup = new Signup;
    return $signup->page();
  }

  /**Signup has been completed**/
  public function completed_signup(){
    $signup = new Signup;
    return $signup->completion_page();
  }

  /**Verify a users account**/
  public function verifyAccount($token){
    $user = User::where('confirmation_token','=',$token)->first();

    if (isset($user)) {
      //Confirm the account
        $user->confirmed = TRUE;
        $user->confirmed_date = time();     
        $user->save();
      //Login the user
        Auth::login($user);
        $data = array(
          'message' => 'confirmed'
        );
    } 
    else {
      $data = array(
        'message' => 'failed',
        'token' => $token
      );
    }

      return View::make('users.confirm')->with($data);
    
  }

  public function confirmAccountPage(){
    return View::make('users.verify');
  }
}