<?php
/**
  * @file Handles Contuct Us Form
  */

class ContactUsController extends BaseController {

  public function getContuctUsForm(){

    $data = Input::all();

    $rules = array (
        'full_names' => 'required',
        'phone_number' => 'required|numeric|min:10',
        'email'  => 'required|email',
        'subject' => 'required|min:5',
        'message' => 'required|min:10',
    );

    $validator  = Validator::make ($data, $rules);

    if ($validator -> passes()){  

      $sendto = getenv('SUPPORT_EMAIL');

      $email_data = array(
          'email' => Input::get('email'),
          'full_names' => Input::get('full_names'),
          'phone_number' => Input::get('phone_number'),
          'subject' => Input::get('subject'),
          'messages' => Input::get('message'),
          '_token' => Input::get('_token'),
      );

      $full_names = Input::get('full_names');
      $email = Input::get('email');

      Mail::send('emails.support', $email_data, function($message) use ($full_names,$sendto,$email)
      {
        $message->to($sendto, 'John Isaji')->cc('john.isaji@apainsurance.org');
        $message->subject('Enquiry / Support');
        $message->from($email, $full_names);
      });
           
      Session::flash('successfull_enquiry', 'We have received your message, we will contact you shortly!');
      return Redirect::to('/contact');

    }
    else{
      return Redirect::to('/contact')
        ->withInput()
        ->withErrors($validator->messages());
    }
  }
}