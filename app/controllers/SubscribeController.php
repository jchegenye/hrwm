<?php

class SubscribeController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 *git
	 * @return Response
	 */

	public function storeEmail() {

	$subscribe = Input::all();

    $rules = array(
       'sEmail' => 'email|unique:Subscribe|required',
    );
    
	  $validator = Validator::make ($subscribe, $rules);
	    if ($validator -> passes()){
	    	// store
	      $Subscribe = new Subscribe;
	      $Subscribe->sEmail = Input::get('sEmail');
	      $Subscribe->save();

	  $data = array(
	  	'sEmail' => Input::get('sEmail')
	  ); 
	   
	  $email = Input::get('sEmail');

    Mail::send('emails.subscribers.subscription', $data, function($message) use ($email)
    {
      $message->from('chegenyejackson@gmail.com', 'Pastor. John Isaji');
      $message->to($email)->subject('Hope And Restoration Worship Ministries, Newsletter Subscription');
    });	

    // redirect
    Session::flash('successfulSubscription', 'We have received your email successfully!');
    return Redirect::to('/#subscribe');

	    } 
	    else {
	      return Redirect::to('/#subscribe')
	          ->withErrors($validator);
	    }

		}

	}