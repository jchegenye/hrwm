<?php 

class WorkbenchController extends BaseController{
  
  /**
 * 
 *
 * @return users,subscribers
 *
 */

  public function home(){
    
    $subscribed = Subscribe::all();
    $member = User::all();

  	$user = Auth::user();

    return View::make('admin/workbench')
      ->with(['user' => $user])
      ->with('countUsers', $user)

      ->with('members', $member)

      ->with('subscribers', $subscribed)
      ->with('countSubscribers', $subscribed);

  }

}