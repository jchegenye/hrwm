<?php
/**
  * @Hold data for all Subscribe's emails at hopeandrestorationworshipminisrties.org
  */
use Jenssegers\Mongodb\Model as Eloquent;


class Subscribe extends Eloquent {

/**
* The database table (collection) used by the model.
*
* @var string
*/
  protected $collection = 'Subscribe';

/**
   * 
   * Set date format 
   *
   */
public function subscriber_date(){
    if ($this->registered) {
      return date('d M Y',(int)$this->registered);
    }
    else {
      return $this->created_at->format('d M Y');
    }

  }

 }