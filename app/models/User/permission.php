<?php
use Jenssegers\Mongodb\Model as Eloquent;

class UserPermission extends Eloquent{
  protected $table = 'permissions';
}