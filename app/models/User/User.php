<?php
/**
  * @handles sign up 
  */
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
/*use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;*/

use Jackchegenye\User\Permission;
use Jackchegenye\User\Traits\Reportable;

use Jenssegers\Mongodb\Model as Eloquent;


class User extends Eloquent implements UserInterface, RemindableInterface  {

  use UserTrait, RemindableTrait;
/**
* The database table (collection) used by the model.
*
* @var string
*/
  protected $collection = 'users';


  /**
     * Get the roles a user has
     */
  public function roles()
  {
        return $this->belongsToMany('Role');
  }

  /**
   * Return an array of the roles the user has.
   */
  public function role_names(){
    $role_ids = $this->role_ids;

    if (count($role_ids) > 0) {
      $roles = array();

      foreach ($role_ids as $role_id) {
        if (Cache::has('role_' . $role_id))
        {
          $role = Cache::get('role_' . $role_id);
        }
        else{
          $role = Role::find($role_id);

          Cache::put('role_' . $role_id, $role, 360);
        }

        array_push($roles, $role->name);
      }

      return $roles;
    }
    else {
      $roles = array();

      return $roles;
    }
  }

/**
   * 
   * Set date format 
   *
   */

  public function signup_date(){
    if ($this->registered) {
      return date('d M Y',(int)$this->registered);
    }
    else {
      return $this->created_at->format('d M Y');
    }

  }



}