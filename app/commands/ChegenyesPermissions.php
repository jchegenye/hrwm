<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Yaml\Yaml;

class ChegenyesPermissions extends Command {

  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'jc:permissions';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Update the user permissions';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function fire()
  {
    $this->info('Updating the permissions. Hold On.');
    
    $file = app_path().'/permissions.yml';
    
    if ( ! file_exists($file))
    {
      $this->error('The file permissions.yml does not exist');
    }
    else{
      //Load the YAML file and parse it.
      $array = Yaml::parse(file_get_contents($file));
      
      foreach ($array as $key => $value) {
        $name = $key;
        //Lets loop through the value array to get the other details.
        $machine_name = $value['name'];
        $description = $value['description'];

        //We need to check if we already stored this permission.
        $permission = UserPermission::where('machine_name','=',$machine_name)->first();

        //Only create a new permission if it is not existing.
        if (empty($permission)) {
          $new_permission = new UserPermission;
          $new_permission->name = $name;
          $new_permission->machine_name = $machine_name;
          $new_permission->description = $description;
          $new_permission->save();

          $this->info('New Permission:'.$name);
        }
      }
    }
  }

}
