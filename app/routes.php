<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', function(){
	return View::make('welcome');
});

Route::get('/about-us', function(){
	return View::make('pages/about');
});

Route::get('/gallery', function(){
  return View::make('pages/gallery');
});

Route::get('/events', function(){
  return View::make('pages/events');
});

Route::get('/testimonial', function(){
  return View::make('pages/testimonial');
});

Route::get('/contact', function(){
  return View::make('pages/contact');
});

Route::get('/test', function(){
  return View::make('test');
});

Route::post('contact-us', [
  'uses' => 'ContactUsController@getContuctUsForm'
]);

/*Route::get('gallery',array(
  'uses' => 'GalleryController@show_gallery'
));*/

$route_partials = [
  //admin
  'admin/admin',
  //Users
  'users/user',
  //Workbench
  'workbench/workbench',
  //subscribe
  'subscribe/subscribe',
];

/*
|---------------------------------------------------
| Route Partial Loadup
|---------------------------------------------------
*/

foreach ($route_partials as $partial) {

    $file = __DIR__.'/routes/'.$partial.'.php';

    if ( ! file_exists($file))
    {
        $msg = "Route partial [{$partial}] not found.";
        throw new \Illuminate\Filesystem\FileNotFoundException($msg);
    }

    require_once $file;
}