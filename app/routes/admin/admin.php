<?php
Route::group(array('prefix' => 'admin','before' => 'auth'), function(){
	
  //Users
  Route::post('user',array(
    'uses'=>'UsersController@storeUser',
    'before' => 'access_everything'
  ));
  
  Route::post('role',array(
    'uses'=>'RoleController@store_role',
    'before'=>'access_everything'
  ));
  
  Route::put('role/{id}',array(
    'uses'=>'RoleController@updatePermission',
    'as'=>'role_update',
    'before'=>'access_everything'
  ));

});
