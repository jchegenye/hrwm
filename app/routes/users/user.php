<?php

/*
|--------------------------------------------------------------------------
| Login Routes
|--------------------------------------------------------------------------
|
| All roots for login.
|
*/
Route::get('login', array(
  'uses' => 'LoginController@showLogin'
));
Route::post('login', array(
  'uses'=>'LoginController@doLogin',
  'before'=>'guest'
));
Route::get('logout', array(
  'uses'=>'LoginController@doLogout',
  'before'=>'auth'
));

/*
|--------------------------------------------------------------------------
| Register / Sign-Up user Routes
|--------------------------------------------------------------------------
|
| All roots for user Sign-Up.
|
*/
/*Route::get('signup',array(
  'uses' => 'SignupController@signup',
  'before' => 'guest',
));*/
Route::get('signup/complete',array(
  'uses' => 'SignupController@completed_signup',
  'before' => 'auth',
));
Route::post('signup/details',array(
  'uses' => 'SignupController@process_details',
  'before' => 'auth',
));
Route::post('signup',array(
  'uses' => 'SignupController@processSignup',
  'before' => 'guest',
));
Route::post('signup/opera',array(
  'uses' => 'SignupController@processOperaSignup',
  'before' => 'guest',
));

Route::get('verify/email',array(
  'uses' => 'SignupController@uniqueEmail',
));
Route::get('verify/phone',array(
  'uses' => 'SignupController@uniquePhone',
));
Route::get('user/confirm/account/',array(
  'uses' => 'SignupController@confirmAccountPage'
));
Route::get('account/verify/{token}',array(
  'uses' => 'SignupController@verifyAccount'
));

/*
|--------------------------------------------------------------------------
| Password Reset Routes
|--------------------------------------------------------------------------
|
| All roots for user Password Reset.
|
*/
Route::get('password/reminder',array(
  'uses' => 'PasswordController@remind'
/*  'before' => 'guest'*/
));

/*
|--------------------------------------------------------------------------
| Access Workbench
|--------------------------------------------------------------------------
|
| All for accessing workbench.
|
*/
Route::get('/workbench/{uid}',array(
  'before' => 'auth|manage_workbench',
  'uses' => 'SubscribeController@showEmails'
));

/*
|--------------------------------------------------------------------------
| Create Roles
|--------------------------------------------------------------------------
|
| All roles and permissions goes here
|
*/
Route::group(array('prefix' => '','before' => 'auth'), function()
{

  Route::get('{id}/roles',array(
    'uses' => 'UsersController@editRoles',
    'before'=>'access_everything',
    'as' => 'edit-roles'
  ));
  Route::put('{id}/role',array(
    'uses'=>'UsersController@storeUserRole',
    'as'=>'user.role.update',
    'before'=>'access_everything'
  ));

});

Route::get('workbench/role/create',array(
  'uses'=>'RoleController@create_role',
  'before'=>'manage_users',
  'as'=>'admin'
));
Route::get('workbench/role/{id}',array(
  'uses'=>'RoleController@showRole',
  'before'=>'manage_users',
  'as'=>'permissions'
));
Route::get('workbench/role/{id}/edit',array(
  'uses'=>'RoleController@editPermission',
  'before'=>'manage_users',
  'as' => 'update-permission'
));
