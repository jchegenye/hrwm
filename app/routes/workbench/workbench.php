<?php 

Route::get('/workbench/{uid}',array(
  'before' => 'auth|access_everything',
  'uses' => 'WorkbenchController@home',
  'as'=>'home'
));

Route::get('/workbench/{uid}/subscribers',array(
  'before' => 'auth|access_everything',
  'uses' => 'WorkbenchController@home',
  'as'=>'subscriber'
));

Route::get('/workbench/{uid}/users',array(
  'before' => 'auth|access_everything',
  'uses' => 'WorkbenchController@home',
  'as'=>'users'
));

Route::get('/workbench/{uid}/write',array(
  'before' => 'auth|access_everything',
  'uses' => 'WorkbenchController@home',
  'as'=>'write'
));