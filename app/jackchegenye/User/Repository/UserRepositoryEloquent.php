<?php namespace Jackchegenye\User\Repository;

/**
 * The hivisasa user repository.
 * The implementation of the methods have been broken down into smaller task specific traits.
 *
 * @version 0.0.1
 *
 * @author Bernard Nandwa <nandwabee@gmail.com>
 * @package Jackchegenye
 */

use Jackchegenye\User\Repository\Contracts\RepositoryInterface;
use Jackchegenye\User\Repository\Eloquent\FindUser;
use Jackchegenye\User\Repository\Eloquent\FindBy;
use Jackchegenye\User\Repository\Eloquent\DeleteUser;
use Jackchegenye\User\Repository\Eloquent\DestroyUser;
use Jackchegenye\User\Repository\Eloquent\PaginateUsers;
use User;

class UserRepositoryEloquent implements RepositoryInterface{
  use FindUser,DeleteUser,DestroyUser,FindBy,PaginateUsers;

  public function create($data){}

  public function update($data,$uid){}

}