<?php namespace Jackchegenye\User\Repository\Contracts;

/**
 * Interface RepositoryInterface
 *
 * Lays down the rules for building the user repository.
 *
 * @package Jackchegenye
 */

interface RepositoryInterface{
  public function paginate();

  public function create($data);

  public function update($data,$uid);

  public function find($uid);

  public function delete($uid);

  public function destroy($uid);

  public function find_by($field,$value);
}