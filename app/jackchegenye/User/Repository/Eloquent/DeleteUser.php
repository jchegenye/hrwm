<?php namespace Jackchegenye\User\Repository\Eloquent;

/**
 * Trait DeleteUser
 *
 * @package Jackchegenye
 */

trait DeleteUser{
  /**
   * @param $uid
   *
   * Given a user uid lets delete the user.
   *
   */
  public function delete($uid){

    return $this->$user->where('uid','=',(int)$uid)->delete();

  }
}