<?php namespace Jackchegenye\User\Repository\Eloquent;

/**
 * Trait FindUser
 *
 * @package Jackchegenye
 */

use User;
use Cache;

trait FindUser{
  /**
   * @param $uid
   *
   * Given a user uid lets find the user.
   *
   * @return object
   */
  public function find($uid){
    //@todo fetch from the  cache instead

    if (Cache::has('user_' . $uid)){
      $user = Cache::get('user_' . $uid);
    }
    else{
      $user = User::where('uid','=',(int)$uid)->first();
    }

    return $user;
  }
}