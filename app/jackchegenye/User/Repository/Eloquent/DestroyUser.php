<?php namespace Jackchegenye\User\Repository\Eloquent;

/**
 * Trait DestroyUser
 *
 * @package Jackchegenye
 */

trait DestroyUser{
  /**
   * @param $uid
   *
   * Given a user uid lets find the user.
   *
   */
  public function destroy($uid){

    return $this->$user->where('uid','=',(int)$uid)->destroy();

  }
}