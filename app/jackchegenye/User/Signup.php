<?php namespace Jackchegenye\User;

use Agent;
use Mail;
use View;
use Request;
use Jackchegenye\User\Signup\CompletedSignup;

class Signup{
  use CompletedSignup;
  /** 
    * Display the signup page. At the moment we want to keep the mobile and desktop separate.
    */
  public function page(){
    if (Agent::isMobile()) {
      if (Agent::version('Opera Mini') || Agent::mobileGrade() == 'C' || Agent::mobileGrade() == 'B') {
        return View::make('users.opera_mini.signup');    
      } 
    }
    return View::make('users.signup');   
  }

  public function followup_email($email){
    $data = array();
    Mail::send('emails.users.shukrani', $data, function($message) use ($email)
    {
      $message->from('chegenyejackson@gmail.com', 'Pastor. John Isaji');
      $message->to($email)->subject('Hope And Restoration Worship Ministries, Church Member');
    });
  }
}