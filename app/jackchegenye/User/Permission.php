<?php namespace Jackchegenye\User;

/**
  * User permissions manager for HopeRestotationWorshipMinistry.com
  */

use Auth;
use User;
use Role;
use UserPermission;
use Cache;

class Permission {
  //Check the current user's permission and return an array.
  public static function check(){
    //Is the user logged in.
    if (Auth::check()) {
      //Which user is this.
      $user = Auth::user();

      //What roles does this user have.
      $roles = $user->role_ids;

      if (count($roles) != 0) {
        $permission_array = [];

        //Each of these roles could be having a permission.
        foreach ($roles as $role_id) {
          if (strlen($role_id) > 2) {
            //Load the Role
            if (Cache::has('role_' . $role_id))
            {
              $role = Cache::get('role_' . $role_id);
            }
            else{
              $role = Role::find($role_id);

              Cache::put('role_' . $role_id, $role, 360);
            }
            


            $permissions = $role->permission_id;

            if (isset($permissions)) {
              for ($i=0; $i < count($permissions); $i++) { 
                if ($permissions[$i] != NULL) {
                  //Load the Permission
                  if (Cache::has('permission_' . $permissions[$i]))
                  {
                    $permission = Cache::get('permission_' . $permissions[$i]);
                  }
                  else{
                    $permission = UserPermission::find($permissions[$i]);

                    Cache::put('permission_' . $permissions[$i], $permission, 360);
                  }

                  $machine_name = $permission->machine_name;

                  //Push all machine_names to the array
                  array_push($permission_array, $machine_name);
                }
              }
            } 
          }         
        }
        //Filter the permission array.
        $filtered_permissions = array_unique($permission_array);

        //Return the filtered array
        return $filtered_permissions;
      }
      else{
        $no_roles = array();

        return $no_roles;
      }     
    }
    else{
      //The user is not signed in.Return an empty array.
      $no_permissions = array();
      
      return $no_permissions;
    }    
    
  }

  /**
    * Return an array of permissions that the user has
    */
  public function permissions(User $user){
    //Get the roles
    $roles = $user->role_ids;

    if (count($roles) == 0) {
      $data = [
        'permission_names' => [],
        'permission_machine_names' => []
      ];

      return $data;
    } 
    else {
      $permission_names = [];
      $permission_machine_names = [];

      foreach ($roles as $role_id) {
        if (Cache::has('role_' . $role_id))
        {
          $role = Cache::get('role_' . $role_id);
        }
        else{
          $role = Role::find($role_id);

          Cache::put('role_' . $role_id, $role, 360);
        }

        $permissions = $role->permission_id;

        if (isset($permissions)) {
          for ($i=0; $i < count($permissions); $i++) { 
            
            if ($permissions[$i] != NULL) {
              $permission = UserPermission::find($permissions[$i]);

              $machine_name = $permission->machine_name;
              $name = $permission->name;

              //Push all machine_names to the array
              array_push($permission_machine_names, $machine_name);
              array_push($permission_names, $name);
            }

          }
        } 
      }

      $data = [
        'permission_names' => $permission_names,
        'permission_machine_names' => $permission_machine_names
      ];

      return $data;
    }
    

  }

  //Get users with this permission
  public function users($permission){
    $permissions = UserPermission::where('machine_name','=',$permission)->first();

    \Log::info($permissions->id);

    $permission_id = [$permissions->id];

    $roles = Role::whereIn('permission_id',$permission_id)->get();

    if (!$roles->isEmpty()) {
      $user_array = [];

      foreach ($roles as $role) {
        //Get the users
        $users = User::whereIn('role_ids',[$role->id])->get();

        foreach ($users as $user) {
          array_push($user_array, $user->id);
        }
        
      }

      return array_unique($user_array);
    }

    return;
  }

}