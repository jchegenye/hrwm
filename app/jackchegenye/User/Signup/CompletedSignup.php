<?php namespace Jackchegenye\User\Signup;

use View;
use Agent;

trait CompletedSignup{
  public function completion_page(){
    if (Agent::isMobile()) {
      if (Agent::version('Opera Mini') || Agent::mobileGrade() == 'C' || Agent::mobileGrade() == 'B') {
        return View::make('users.opera_mini.confirmation');    
      } 
    }

    return View::make('users.signup.complete');
  }
}