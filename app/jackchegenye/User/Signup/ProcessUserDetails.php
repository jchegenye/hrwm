<?php namespace Jackchegenye\User\Signup;

use Validator;
use Input;
use Redirect;
use Auth;
use Agent;
use View;

trait ProcessUserDetails{
  public function process_details(){
    //We need to at least have the uid before proceeding
    $rules = [
      'phone' => 'unique:users|required|max:10',
      'name' => 'unique:users|required|min:2|max:25'
    ];

    $validator = Validator::make(Input::all(),$rules);

    if ($validator->fails()) {
      return Redirect::to('signup/complete')
      ->withErrors($validator)
      ->withInput(Input::all());
    } 
    else {
      //Set the name and phone
      $user = Auth::user();
      $user->name = Input::get('name');
      $user->phone = Input::get('phone');
      $user->save();

      if (Agent::isDesktop()) {
        return View::make('layouts.user.thank-you-sign-up');
      }
      return Redirect::to('reporter/' . $user->uid . '?newuser=true');
    }
    
  }
}