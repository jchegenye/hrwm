<?php namespace Jackchegenye\User\Signup;

use Validator;
use Response;
use Input;
use Crypt;
use User;
use Role;
use Queue;
use Mail;
use Request;
use Hash;
use URL;
use Auth;
use Redirect;
use Agent;
use Event;

trait ProcessSignup{
  
  public function processSignup(){
    //Lets validate user inputs.
    $rules = [
      'username' => 'required|unique:users|min:3|max:15', 
      'email' => 'email|unique:users|required',
      'password' => 'required|unique:users|min:8|max:25',
      /*'g-recaptcha-response' => 'required',*/
    ];

    $validator = Validator::make(Input::all(),$rules);

    if ($validator->fails()) {
      $error = [
        'message' => 'error',
        'message_description' => 'There were errors in your form.',
        'errors' => $validator->messages()
      ];
     
      return Redirect::to('signup')
        ->withErrors($validator)
        ->withInput(Input::all());
    }

    //Lets validate our form using Re-captcha
/*      $recaptcha = new \ReCaptcha\ReCaptcha(getenv('RECAPTCHA_SECRET'));
      $recaptcha_response = $recaptcha->verify(Input::get('g-recaptcha-response'), $_SERVER['REMOTE_ADDR']);*/
   /* if ($recaptcha_response->isSuccess())*/ 
   
   else {
      $users = User::orderBy('uid', 'DESC')->take(1)->get();

      $confirmation_token = Crypt::encrypt(Input::get('email'));

      //Go ahead and create a new user
      $user = new User;
      $user->username = Input::get('username');
      $user->email = Input::get('email');
      $user->password = Hash::make(Input::get('password'));
      $user->confirmation_token = $confirmation_token;
    } 
/*    else{

      Session::flash('unsuccessful_signup', 'Kindly fill the ReCaptcha to verify if you are a roboot!');
        return Redirect::to('/signup');
    }*/

    if ($users->isEmpty()) {
          $user->uid=1;
        }
        else {
          foreach ($users as $account) {
            $uid = $account->uid;

            $user->uid = $uid+1;
          }
        }

    //Always assign a role of fellowship.
    $role = Role::where('name','=','Fellowship')->first();
    $user->roles()->attach($role);
    $user->save();

    //Send them a welcome email
    $email_data = array(
      'confirmation_token' => $confirmation_token,
      'confirm_url' => URL::to('/') . '/account/verify/' . $confirmation_token,
    );

    $email = Input::get('email');

    Mail::send('emails.users.signup', $email_data, function($message) use ($email)
    {
      $message->to($email)->subject('Welcome to Hope and Restoration Worship Ministries');
    });

    //Login the user immediately
        Auth::login($user);

        return Redirect::to('/signup/complete');

  } 
}
