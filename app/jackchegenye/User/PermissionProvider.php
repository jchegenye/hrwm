<?php namespace Jackchegenye\User;

use Illuminate\Support\ServiceProvider;
/*use Jackchegenye\System\Events\UserEventsHandler;
use Event;*/

class PermissionProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['permission'] = $this->app->share(function($app)
        {
            return new Permission;
        });

        // Shortcut so developers don't need to add an Alias in app/config/app.php
        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Permission', 'Jackchegenye\User\Facades\Permission');
        });
    }

/*    public function boot(){
        //Subscribe to a signup event
        $handler = new UserEventsHandler;
        
        Event::subscribe($handler);
    }*/
}