## hopeandrestorationministries.Com
This is the main code repository for http://www.hopeandrestorationministries.com

QUICK SETUP
------------
This codebase relies on a number of open source projects for it to function as expected. This guide assumes a Linux based environment (Ubuntu).

You will need the following set up on your development machine;
- [Apache](http://httpd.apache.org/) webserver. Preferably 2.4
- [MongoDb](http://http.mongodb.org/) v2.6
- [PHP MongoDB Driver](http://docs.mongodb.org/ecosystem/drivers/php/)

After you have the above requirements installed you can follow through the following installation procedure.

1. Clone this repository from Bitbucket.
2. Clone the layouts from the [Bitbucket repository](https://bitbucket.org/hopeandrestorationministries/layouts) so that it sits in the same directory with this repo.

This project was developed by...
	- Chegenye Asumu Jackson

	Email: chegenyejackson@gmail.com
	Contact: +254-711-494-289
	Contry: Kenya
